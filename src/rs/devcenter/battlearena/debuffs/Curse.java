package rs.devcenter.battlearena.debuffs;

import rs.devcenter.battlearena.heroes.Hero;

/*
 * Curse is a debuff thats the complete opposite of Bless, Max and Current HP are reduced by 100!
 *
 * @author Aleksa Cakic
 * */
public class Curse extends Debuffs {
	public Curse(Hero hero1, Hero hero2) {
		this.setHero1(hero1);
		this.setHero2(hero2);
		this.setName("Curse");
	}

	public Curse(Hero hero) {
		this.setHero1(hero);
		this.setName("Curse");
	}

	@Override
	public void debuff(Hero hero) {
		
			hero.setMaxHealth(hero.getMaxHealth() - 100);
			hero.setCurrentHealth(hero.getCurrentHealth() - 100);
		
	}

	@Override
	public void arenaDebuff(Hero hero1, Hero hero2) {
		getHero1().setMaxHealth(getHero1().getMaxHealth() - 100);
		getHero1().setCurrentHealth(getHero1().getCurrentHealth() - 100);
		getHero2().setMaxHealth(getHero2().getMaxHealth() - 100);	
		getHero2().setCurrentHealth(getHero2().getCurrentHealth() - 100);
	}

	@Override
	public void debuffStrenght(int stren) {
		getHero1().setMaxHealth(getHero1().getMaxHealth() - stren);
		getHero1().setCurrentHealth(getHero1().getCurrentHealth() - stren);
		getHero2().setMaxHealth(getHero2().getMaxHealth() - stren);
		getHero2().setCurrentHealth(getHero2().getCurrentHealth() - stren);
	}
}
