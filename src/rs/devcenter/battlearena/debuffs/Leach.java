package rs.devcenter.battlearena.debuffs;

import rs.devcenter.battlearena.heroes.Hero;

/*
 * Leach is classical mana drain skill.
 *
 * @author Aleksa Cakic
 * */
public class Leach extends Debuffs {

	public Leach(Hero hero1, Hero hero2) {
		this.setHero1(hero1);
		this.setHero2(hero2);
		this.setName("Leach");
	}

	public Leach(Hero hero) {
		this.setHero1(hero);
		this.setName("Leach");
	}

	@Override
	public void debuff(Hero hero) {
		hero.setMaxMana(hero.getMaxMana() - 100);
		hero.setCurrentMana(hero.getCurrentMana() - 100);
	}

	@Override
	public void arenaDebuff(Hero hero1, Hero hero2) {
		// no arena has leach debuff
	}

	@Override
	public void debuffStrenght(int stren) {
		getHero1().setMaxHealth(getHero1().getMaxHealth() - stren);
		getHero1().setCurrentHealth(getHero1().getCurrentHealth() - stren);
		getHero2().setMaxHealth(getHero2().getMaxHealth() - stren);
		getHero2().setCurrentHealth(getHero2().getCurrentHealth() - stren);
	}
}
