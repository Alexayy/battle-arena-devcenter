package rs.devcenter.battlearena.debuffs;

import rs.devcenter.battlearena.heroes.Hero;
import rs.devcenter.battlearena.spells.Spells;
/*
* Abstract class from which Curse, Damage Over Time and Leach inherit properties.
*
* @author Aleksa Cakic
* */
public abstract class Debuffs {

	private String name;
	private Hero hero1, hero2;
	private Spells spell;

	public abstract void arenaDebuff(Hero hero1, Hero hero2);

	public abstract void debuffStrenght(int stren);

	public abstract void debuff(Hero hero);

	public Spells getSpell() {
		return spell;
	}

	public void setSpell(Spells spell) {
		this.spell = spell;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Hero getHero1() {
		return hero1;
	}

	public void setHero1(Hero hero1) {
		this.hero1 = hero1;
	}

	public Hero getHero2() {
		return hero2;
	}

	public void setHero2(Hero hero2) {
		this.hero2 = hero2;
	}
}
