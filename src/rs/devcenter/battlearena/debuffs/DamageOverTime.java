package rs.devcenter.battlearena.debuffs;

import rs.devcenter.battlearena.heroes.Hero;
/*
* This is not a classical DoT, because i hate everything related to these kinds of debuffs,
* in any game, and in my game. This debuff is exclusive as a turn debuff for a battle mode.
*
* @author Aleksa Cakic
* */
public class DamageOverTime extends Debuffs {

	public DamageOverTime(Hero hero1, Hero hero2) {
		this.setHero1(hero1);
		this.setHero2(hero2);
		this.setName("Damage Over Time");
	}

	public DamageOverTime(Hero hero) {
		this.setHero1(hero);
		this.setName("Damage Over Time");
	}

	@Override
	public void debuff(Hero hero) { // This is the method where the debuff ticks
			hero.setCurrentMana(hero.getCurrentMana() - 50);
			hero.setCurrentHealth(hero.getCurrentHealth() - 50);
	}

	@Override
	public void arenaDebuff(Hero hero1, Hero hero2) {
		getHero1().setMaxHealth(getHero1().getCurrentHealth() - 50);
		getHero2().setMaxHealth(getHero2().getCurrentHealth() - 50);
	}

	@Override
	public void debuffStrenght(int stren) {
		getHero1().setMaxHealth(getHero1().getMaxHealth() - stren);
		getHero1().setCurrentHealth(getHero1().getCurrentHealth() - stren);
		getHero2().setMaxHealth(getHero2().getMaxHealth() - stren);
		getHero2().setCurrentHealth(getHero2().getCurrentHealth() - stren);
	}
}
