package rs.devcenter.battlearena.heroes;

import java.util.Random;

import rs.devcenter.battlearena.spells.DazingStrike;
import rs.devcenter.battlearena.spells.EmpyreanProvidence;
import rs.devcenter.battlearena.spells.ShieldBash;

/*
 * Dangerous when their friends are threathened. Dare not to provoke them or their friends... or clansmates...
 * or any one that they care about.
 *
 * High survivabilty, low damage.
 *
 * @author Aleksa Cakic
 * */

public class Templar extends Hero {

	public Templar(String name, int currentXp, int currentLevel) {

		this.setName(name);
		this.setCurrentXp(currentXp);
		this.setCurrentLevel(currentLevel);
		this.setPlayableClass("Templar");
		this.setMaxHealth(750 + 100 * getCurrentLevel());
		this.setCurrentHealth(750 + 100 * getCurrentLevel());
		this.setCurrentMana(100 + 20 * getCurrentLevel());
		this.setMinimumDmg(10 + getCurrentLevel());
		this.setMaximumDmg(50 + 2 * getCurrentLevel());
		this.setCritStrikeChance(20);
		this.setHeroSpells(getHeroSpells());
		this.setHeroDebuffs(getHeroDebuffs());
		
		DazingStrike ds = new DazingStrike(this);
		ShieldBash sb = new ShieldBash(this);
		EmpyreanProvidence ep = new EmpyreanProvidence(this);
		
		this.setSpell1(ds);
		this.setSpell2(sb);
		this.setSpell3(ep);
		
		this.setHeroDebuffs(getHeroDebuffs());
		this.setHeroBuffs(getHeroBuffs());

	}

	public String getSkillList() { // skill list for flavor text
		return "Dazing Strike, " + "Shield Bash, " + "Empyrean Armor, " + "Empyrean Providence ";
	}

	@Override
	void critStrike() {
		Random rand = new Random();
		rand.nextInt(getCritStrikeChance() + 100);
		if (rand.nextInt() > 100)
			this.setMaximumDmg(getMaximumDmg() * 2);
	}
}
