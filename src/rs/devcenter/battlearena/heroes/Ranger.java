package rs.devcenter.battlearena.heroes;

import java.util.Random;

import rs.devcenter.battlearena.spells.AnkleShot;
import rs.devcenter.battlearena.spells.BullsEye;
import rs.devcenter.battlearena.spells.ThroatShot;

/*
 * Swift and nimble, rangers were always somewhere in the wild playing with animals, and feeding them
 * their enemies that dared to insult mother nature.
 *
 * @author Aleksa Cakic
 * */

public class Ranger extends Hero {

	public Ranger(String name, int currentXp, int currentLevel) {

		this.setName(name);
		this.setCurrentXp(currentXp);
		this.setCurrentLevel(currentLevel);
		this.setPlayableClass("Ranger");
		this.setMaxHealth(350 + 50 * getCurrentLevel());
		this.setCurrentHealth(350 + 50 * getCurrentLevel());
		this.setCurrentMana(200 + 20 * getCurrentLevel());
		this.setMinimumDmg(20 + getCurrentLevel());
		this.setMaximumDmg(60 + 2 * getCurrentLevel());
		this.setCritStrikeChance(75);
		this.setHeroSpells(getHeroSpells());
		this.setHeroDebuffs(getHeroDebuffs());

		ThroatShot ts = new ThroatShot(this);
		AnkleShot as = new AnkleShot(this);
		BullsEye be = new BullsEye(this);

		this.setSpell1(ts);
		this.setSpell2(as);
		this.setSpell3(be);
	}
	public String getSkillList() { // skill list for flavour text
		return "Throath Shot, " + "Ankle Shot, " + "Knee Shot, " + "Bullseye ";
	}

	@Override
	void critStrike() {
		Random rand = new Random();
		rand.nextInt(getCritStrikeChance() + 101);
		if (rand.nextInt() > 100)
			this.setMaximumDmg(getMaximumDmg() * 2);
	}
}
