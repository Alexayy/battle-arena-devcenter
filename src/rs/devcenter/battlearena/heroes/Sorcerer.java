package rs.devcenter.battlearena.heroes;

import java.util.Random;

import rs.devcenter.battlearena.spells.ArcaneBlast;
import rs.devcenter.battlearena.spells.ArcaneContract;
import rs.devcenter.battlearena.spells.ArcaneMissile;

/*
 * Paper thin, but packs a massive burst of arcane energy. Since birth, they have been unstable with energy...
 * and personality...
 *
 * @author Aleksa Cakic
 * */

public class Sorcerer extends Hero {

	public Sorcerer(String name, int currentXp, int currentLevel) {

		this.setName(name);
		this.setCurrentXp(currentXp);
		this.setCurrentLevel(currentLevel);
		this.setPlayableClass("Sorcerer");
		this.setMaxHealth(200 + 50 * getCurrentLevel());
		this.setCurrentHealth(200 + 50 * getCurrentLevel());
		this.setCurrentMana(600 + 50 * getCurrentLevel());
		this.setMinimumDmg(10 + getCurrentLevel());
		this.setMaximumDmg(20 + 4 * getCurrentLevel());
		this.setCritStrikeChance(10);
		this.setHeroSpells(getHeroSpells());
		this.setHeroDebuffs(getHeroDebuffs());

		ArcaneBlast ab = new ArcaneBlast(this);
		ArcaneMissile am = new ArcaneMissile(this);
		ArcaneContract ac = new ArcaneContract(this);

		this.setSpell1(ab);
		this.setSpell2(am);
		this.setSpell3(ac);
		this.setHeroBuffs(getHeroBuffs());
	}

	public String getSkillList() { // skill list for flavour text
		return "Arcane Blast, " + "Arcane Missile, " + "Arcane Focus, " + "Arcane Contract ";
	}

	@Override
	void critStrike() {
		Random rand = new Random();
		rand.nextInt(getCritStrikeChance() + 101);
		if (rand.nextInt() > 100)
			this.setMaximumDmg(getMaximumDmg() * 2);
	}
}
