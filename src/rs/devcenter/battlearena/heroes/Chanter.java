package rs.devcenter.battlearena.heroes;

import java.util.Random;

import rs.devcenter.battlearena.spells.*;

/*
 * Chanters are braver Clerics. Instead of staying in last rows, Chanters go for front rows,
 * where they share battlefield back-to-back with their friends in need.
 * Low damage, high sustain.
 *
 * @author Aleksa Cakic
 * */
public class Chanter extends Hero {

	public Chanter(String name, int currentXp, int currentLevel) {

		this.setName(name);
		this.setCurrentXp(currentXp);
		this.setCurrentLevel(currentLevel);
		this.setPlayableClass("Chanter");
		this.setMaxHealth(450 + 50 * getCurrentLevel());
		this.setCurrentHealth(450 + 50 * getCurrentLevel());
		this.setCurrentMana(150 + 50 * getCurrentLevel());
		this.setMaxMana(150 + 50 * getCurrentLevel());
		this.setMinimumDmg(40 + getCurrentLevel());
		this.setMaximumDmg(60 + 2 * getCurrentLevel());
		this.setCritStrikeChance(60);
		this.setHeroSpells(getHeroSpells());
		this.setHeroDebuffs(getHeroDebuffs());

		BlessingOfStone bos = new BlessingOfStone(this);
		RecoverySpell rs = new RecoverySpell(this);
		DisorientingBlow disBlow = new DisorientingBlow(this);

		this.setSpell1(bos);
		this.setSpell2(rs);
		this.setSpell3(disBlow);

		this.setHeroDebuffs(getHeroDebuffs());
		this.setHeroBuffs(getHeroBuffs());
		this.setHeroBuffs(getHeroBuffs());
		this.setClassDescription("Chanters are Clerics, but with a little twist - Chanters are always in first rows, "
				+ "\nprotecting real Heroes, like real Heroes they are!");
	}

	public String getSkillList() { // skill list for flavour text
		return "Blessing of Stone, " + "Recovery Spell, " + "Disorienting Blow, " + "Yustiels Grace ";
	}

	@Override
	void critStrike() {
		Random rand = new Random();
		rand.nextInt(getCritStrikeChance() + 101);
		if (rand.nextInt() > 50)
			this.setMaximumDmg(getMaximumDmg() * 2);
	}
}
