package rs.devcenter.battlearena.heroes;

import java.util.Random;

import rs.devcenter.battlearena.spells.BerserkersDash;
import rs.devcenter.battlearena.spells.Rage;
import rs.devcenter.battlearena.spells.RelentlessBlow;

/*
 * Gladiators are brave warriors who spent their life fighting. A good gladiator has honor,
 * a good appetite and loves LOADS of beer!
 * High Damage, medium sustain
 *
 * @author Aleksa Cakic
 * */
public class Gladiator extends Hero {

	public Gladiator(String name, int currentXp, int currentLevel) {

		this.setName(name);
		this.setCurrentXp(currentXp);
		this.setCurrentLevel(currentLevel);
		this.setPlayableClass("Gladiator");
		this.setMaxHealth(500 + 50 * getCurrentLevel());
		this.setCurrentHealth(500 + 50 * getCurrentLevel());
		this.setCurrentMana(100 + 20 * getCurrentLevel());
		this.setMinimumDmg(40 + getCurrentLevel());
		this.setMaximumDmg(80 + 2 * getCurrentLevel());
		this.setCritStrikeChance(50);
		this.setHeroSpells(getHeroSpells());
		this.setHeroDebuffs(getHeroDebuffs());

		BerserkersDash bd = new BerserkersDash(this);
		RelentlessBlow rb = new RelentlessBlow(this);
		Rage rage = new Rage(this);

		this.setSpell1(bd);
		this.setSpell2(rb);
		this.setSpell3(rage);

		this.setHeroBuffs(getHeroBuffs());
		this.setHeroBuffs(getHeroBuffs());
		this.setHeroBuffs(getHeroBuffs());
	}

	public String getSkillList() { // skill list for flavour text
		return "Berserkers Dash, " + "Relentless Blow, " + "Charge, " + "Rage ";
	}

	@Override
	void critStrike() {
		Random rand = new Random();
		rand.nextInt(getCritStrikeChance() + 101);
		if (rand.nextInt() > 100)
			this.setMaximumDmg(getMaximumDmg() * 2);
	}
}
