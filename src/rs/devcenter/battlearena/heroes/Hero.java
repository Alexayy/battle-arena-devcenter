package rs.devcenter.battlearena.heroes;
/*
* Hero is abstract class from which other playable classes will inherit its properties.
*
* @author Aleksa Cakic
* */
import rs.devcenter.battlearena.buffs.Buffs;
import rs.devcenter.battlearena.debuffs.Debuffs;
import rs.devcenter.battlearena.spells.Spells;

public abstract class Hero {
	private String name;
	private int maxHealth;
	private int currentHealth;
	private int maxMana;
	private int currentMana;
	private int minimumDmg;
	private int maximumDmg;
	private int critStrikeChance;
	private int maxLevel;
	private int currentLevel = 1;
	private int maximumXp = 50 + currentLevel * 10;
	private int currentXp;
	private String classDescription;
	private String playableClass;

	private Spells heroSpells;
	private Buffs heroBuffs;
	private Debuffs heroDebuffs;

	private Spells spell1;
	private Spells spell2;
	private Spells spell3;

	abstract void critStrike();

	public String getPlayableClass() {
		return playableClass;
	}

	public void setPlayableClass(String playableClass) {
		this.playableClass = playableClass;
	}

	public Spells getHeroSpells() {
		return heroSpells;
	}

	public void setHeroSpells(Spells heroSpells) {
		this.heroSpells = heroSpells;
	}

	public Buffs getHeroBuffs() {
		return heroBuffs;
	}

	public void setHeroBuffs(Buffs heroBuffs) {
		this.heroBuffs = heroBuffs;
	}

	public Debuffs getHeroDebuffs() {
		return heroDebuffs;
	}

	public void setHeroDebuffs(Debuffs heroDebuffs) {
		this.heroDebuffs = heroDebuffs;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMaxHealth() {
		return maxHealth;
	}

	public void setMaxHealth(int maxHealth) {
		this.maxHealth = maxHealth;
	}

	public int getCurrentHealth() {
		return currentHealth;
	}

	public void setCurrentHealth(int currentHealth) {
		this.currentHealth = currentHealth;
	}

	public int getMaxMana() {
		return maxMana;
	}

	public void setMaxMana(int maxMana) {
		this.maxMana = maxMana;
	}

	public int getCurrentMana() {
		return currentMana;
	}

	public void setCurrentMana(int currentMana) {
		this.currentMana = currentMana;
	}

	public int getMinimumDmg() {
		return minimumDmg;
	}

	public void setMinimumDmg(int minimumDmg) {
		this.minimumDmg = minimumDmg;
	}

	public int getMaximumDmg() {
		return maximumDmg;
	}

	public void setMaximumDmg(int maximumDmg) {
		this.maximumDmg = maximumDmg;
	}

	public int getCritStrikeChance() {
		return critStrikeChance;
	}

	public void setCritStrikeChance(int critStrikeChance) {
		this.critStrikeChance = critStrikeChance;
	}

	public int getMaxLevel() {
		return maxLevel;
	}

	public void setMaxLevel(int maxLevel) {
		this.maxLevel = maxLevel;
	}

	public int getCurrentLevel() {
		return currentLevel;
	}

	public void setCurrentLevel(int currentLevel) {
		this.currentLevel = currentLevel;
	}

	public int getMaximumXp() {
		return maximumXp;
	}

	public void setMaximumXp(int maximumXp) {
		this.maximumXp = maximumXp;
	}

	public int getCurrentXp() {
		return currentXp;
	}

	public void setCurrentXp(int currentXp) {
		this.currentXp = currentXp;
	}

	public String getClassDescription() {
		return classDescription;
	}

	public void setClassDescription(String classDescription) {
		this.classDescription = classDescription;
	}

	public Spells getSpell1() {
		return spell1;
	}

	public void setSpell1(Spells spell1) {
		this.spell1 = spell1;
	}

	public Spells getSpell2() {
		return spell2;
	}

	public void setSpell2(Spells spell2) {
		this.spell2 = spell2;
	}

	public Spells getSpell3() {
		return spell3;
	}

	public void setSpell3(Spells spell3) {
		this.spell3 = spell3;
	}
}
