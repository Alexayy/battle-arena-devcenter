package rs.devcenter.battlearena.combat;

import rs.devcenter.battlearena.heroes.Hero;
import java.util.Random;
import java.util.Scanner;
/*
* This is the class that makes the game playable.
*
* @author Aleksa Cakic
* */
public class PlayerVsOpponent extends Combat {
	
	public PlayerVsOpponent (Hero hero1, Hero hero2) {
		this.setHero1(hero1);
		this.setHero2(hero2);
		this.setRandom(new Random());
		this.setInput(new Scanner(System.in));
	}

    @Override
    public void player() {

        System.out.println("Press 1 to cast " + getHero1().getSpell1().getName() +
                ", press 2 to cast " + getHero1().getSpell2().getName() +
                ", press 3 to cast " + getHero1().getSpell3().getName());

        int a = getInput().nextInt();
        /*

        * This switch statement lets player pick which spell will his class cast, and that spell may
        * or may not have a buff/debuff as a propertie.
        *
        * */
        switch (a) {

            case 1:
                getHero1().getSpell1().spell(getHero2()); // Spell takes a target to be caster on.
                getHero1().getSpell1().spellBuffer(getHero1()); // If the spell has a buff, it will trigger it on the caster.
                getHero1().getSpell1().spellDebuffer(getHero2()); // If the spell has a debuff, it will trigger debuff on the target.
                break;
            case 2:
                getHero1().getSpell2().spell(getHero2()); // Spell takes a target to be caster on.
                getHero1().getSpell2().spellBuffer(getHero1()); // If the spell has a buff, it will trigger it on the caster.
                getHero1().getSpell2().spellDebuffer(getHero2()); // If the spell has a debuff, it will trigger debuff on the target.
                break;
            case 3:
                getHero1().getSpell3().spell(getHero2()); // Spell takes a target to be caster on.
                getHero1().getSpell3().spellBuffer(getHero1()); // If the spell has a buff, it will trigger it on the caster.
                getHero1().getSpell3().spellDebuffer(getHero2()); // If the spell has a debuff, it will trigger debuff on the target.
                break;

                default:
                    System.out.println("Invalid input");
        }

       getHero1().setCurrentMana(getHero1().getCurrentMana() + 50 * getHero1().getCurrentLevel()); // Each new turn gives 50 more mana
       
       	System.out.println(getHero1().getName() + "\nHP: " + getHero1().getCurrentHealth() + "\nMP: " + getHero1().getCurrentMana());
        System.out.println(getHero2().getName() + "\nHP: " + getHero2().getCurrentHealth() + "\nMP: " + getHero2().getCurrentMana());
        System.out.println(" ♦ ♣ ♣ ♣ ♣ ♣ ♦ ♣ ♣ ♣ ♣ ♣ ♦");
    }


    @Override
    public void opponent() {

        int a = getRandom().nextInt(2) + 1;
switch (a) {
    case 1:
        getHero2().getSpell1().spell(getHero1()); // Spell takes a target to be caster on
        getHero2().getSpell1().spellDebuffer(getHero1()); // If the spell has a buff, it will trigger it on the caster.
        getHero2().getSpell1().spellBuffer(getHero2()); // If the spell has a debuff, it will trigger debuff on the target.
        break;
    case 2:
        getHero2().getSpell2().spell(getHero1()); // Spell takes a target to be caster on
        getHero2().getSpell2().spellDebuffer(getHero1()); // If the spell has a buff, it will trigger it on the caster.
        getHero2().getSpell2().spellBuffer(getHero2()); // If the spell has a debuff, it will trigger debuff on the target.
        break;
    case 3:
        getHero2().getSpell3().spell(getHero1()); // Spell takes a target to be caster on
        getHero2().getSpell3().spellDebuffer(getHero1()); // If the spell has a buff, it will trigger it on the caster.
        getHero2().getSpell3().spellBuffer(getHero2()); // If the spell has a debuff, it will trigger debuff on the target.
        break;
    default:
        System.out.println("Computers can't go wrong, unless i'm coding it");
        break;
}
        getHero2().setCurrentMana(getHero2().getCurrentMana() + 50 * getHero2().getCurrentLevel());
    }
}
