package rs.devcenter.battlearena.combat;

import rs.devcenter.battlearena.Modes.Mode;
import rs.devcenter.battlearena.heroes.Hero;

import java.util.Random;
import java.util.Scanner;
/*
* Combat is an abstact class which will be inherited by PlayerVsOpponent class that dictates the games flow.
*
* @author Aleksa Cakic
* */
public abstract class Combat {

	private Hero hero1, hero2;
	private Random random;
	private Scanner input;

	public abstract void player();
	public abstract void opponent();

	public Hero getHero1() {
		return hero1;
	}

	public void setHero1(Hero hero1) {
		this.hero1 = hero1;
	}

	public Hero getHero2() {
		return hero2;
	}

	public void setHero2(Hero hero2) {
		this.hero2 = hero2;
	}

	public Random getRandom() {
		return random;
	}

	public void setRandom(Random random) {
		this.random = random;
	}

	public Scanner getInput() {
		return input;
	}

	public void setInput(Scanner input) {
		this.input = input;
	}
}
