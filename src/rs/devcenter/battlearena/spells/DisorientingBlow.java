package rs.devcenter.battlearena.spells;

import rs.devcenter.battlearena.debuffs.Debuffs;
import rs.devcenter.battlearena.debuffs.Leach;
import rs.devcenter.battlearena.heroes.Hero;

/*
 * Chanter, Leach should be enough to compensate for lack of damage
 *
 * @author Aleksa Cakic
 * */
public class DisorientingBlow extends Spells {

	public DisorientingBlow(Hero hero1) {
		this.setHero1(hero1);
		this.setName("Disorienting Blow");
		this.setManaCost(100 + hero1.getCurrentLevel() * 2);
		this.setDamage(30 + hero1.getMaximumDmg() + hero1.getCurrentLevel() * 2);
	}

	@Override
	public void spell(Hero hero2) {

		if (hero().getCurrentMana() > getManaCost()) {
			hero().setCurrentMana(hero().getCurrentMana() - getManaCost());
			hero2.setCurrentHealth(hero2.getCurrentHealth() - getDamage());
			System.out.println(hero().getName() + " smacks " + hero2.getName() + " and does "
					+ getDamage() + " damage!");
		} else {
			System.out.println("Insuficient mana");
			hero2.setCurrentHealth(hero2.getCurrentHealth() - hero().getMinimumDmg());
		}
	}

	@Override
	public void spellBuffer(Hero hero) {
		// This spell has no Buffs
	}

	@Override
	public void spellDebuffer(Hero opponent) {
		System.out.println("This spell is leaching powers from others!");
		Debuffs debuff = new Leach(getHero2());
		debuff.debuff(opponent);
	}
}
