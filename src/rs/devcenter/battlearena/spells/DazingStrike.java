package rs.devcenter.battlearena.spells;

import rs.devcenter.battlearena.heroes.Hero;

/*
 * Templar, small damage
 *
 * @author Aleksa Cakic
 * */

public class DazingStrike extends Spells {

	public DazingStrike(Hero hero1) {
		this.setHero1(hero1);
		this.setName("Dazing Strike");
		this.setManaCost(40 + hero1.getCurrentLevel() * 2);
		this.setDamage(25 + hero1.getMaximumDmg() + hero1.getCurrentLevel() * 2);
	}

	@Override
	public void spell(Hero hero2) {

		if (hero().getCurrentMana() > getManaCost()) {
			hero().setCurrentMana(hero().getCurrentMana() - getManaCost());
			hero2.setCurrentHealth(hero2.getCurrentHealth() - getDamage());
			System.out.println(hero().getName() + " diced " + hero2.getName() + " and damaged for "
					+ getDamage() + " damage!");
		} else {
			System.out.println("Insuficient mana");
			hero2.setCurrentHealth(hero2.getCurrentHealth() - hero().getMinimumDmg());
		}
	}

	@Override
	public void spellBuffer(Hero hero) {
		// This spell has no buff

	}

	@Override
	public void spellDebuffer(Hero hero) {
		// This spell has no debuff

	}

}
