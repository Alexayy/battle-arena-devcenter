package rs.devcenter.battlearena.spells;

import rs.devcenter.battlearena.buffs.Buffs;
import rs.devcenter.battlearena.debuffs.Debuffs;
import rs.devcenter.battlearena.heroes.Hero;
/*
* This abstract class is the blueprint for all spells that are castable with connected coresponding debuffs and buffs.
*
* @author Aleksa Cakic
* */
public abstract class Spells {

	private Hero hero1, hero2;
	private String name;
	private int damage;
	private int manaCost;
	private int heal;
	private int cooldown;
	private Buffs buffe;
	private Debuffs debuff;
	private String description;

	public abstract void spell(Hero hero);
	public abstract void spellBuffer(Hero hero);
	public abstract void spellDebuffer(Hero hero);

	public int getCooldown() {
		return cooldown;
	}

	public void setCooldown(int cooldown) {
		this.cooldown = cooldown;
	}

	public Hero hero() {
		return hero1;
	}

	public void setHero1(Hero hero1) {
		this.hero1 = hero1;
	}

	public Hero getHero2() {
		return hero2;
	}

	public void setHero2(Hero hero2) {
		this.hero2 = hero2;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public int getManaCost() {
		return manaCost;
	}

	public void setManaCost(int manaCost) {
		this.manaCost = manaCost;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getHeal() {
		return heal;
	}

	public void setHeal(int heal) {
		this.heal = heal;
	}

	public Debuffs getDebuff() {
		return debuff;
	}

	public void setDebuff(Debuffs debuff) {
		this.debuff = debuff;
	}

}
