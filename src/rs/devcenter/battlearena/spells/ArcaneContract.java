package rs.devcenter.battlearena.spells;

import rs.devcenter.battlearena.buffs.Buffs;
import rs.devcenter.battlearena.buffs.Empowerement;
import rs.devcenter.battlearena.heroes.Hero;

/*
 * Sorcerer, self buff for more damage
 *
 * @author Aleksa Cakic
 * */
public class ArcaneContract extends Spells {

	public ArcaneContract(Hero hero1) {
		this.setHero1(hero1);
		this.setName("Arcane Contract");
		this.setManaCost(200 + hero1.getCurrentLevel() * 2);
	}

	@Override
	public void spell(Hero hero) {
		if (hero().getCurrentMana() > getManaCost()) {
			hero().setCurrentMana(hero().getCurrentMana() - getManaCost());
			hero().setMaximumDmg(100);
			System.out.println(hero().getName() + " feels the surge! All of the effects are boosted: "
					+ hero().getMaximumDmg());
		} else {
			System.out.println("Insuficient mana");
			hero.setCurrentHealth(hero.getCurrentHealth() - hero().getMinimumDmg());
		}
	}

	@Override
	public void spellBuffer(Hero hero) {
		Buffs buffed = new Empowerement(hero);
		buffed.setAmount(100);
		buffed.buff(hero());
	}

	@Override
	public void spellDebuffer(Hero hero) {
		// spell has no debuffs
	}
}
