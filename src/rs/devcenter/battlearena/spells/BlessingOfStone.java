package rs.devcenter.battlearena.spells;

import rs.devcenter.battlearena.buffs.Buffs;
import rs.devcenter.battlearena.buffs.Shield;
import rs.devcenter.battlearena.heroes.Hero;

/*
 * Chanter, moderately strong heal, minor damage reflect
 *
 * @author Aleksa Cakic
 * */

public class BlessingOfStone extends Spells {

	public BlessingOfStone(Hero hero1) {
		this.setHero1(hero1);
		this.setName("Blessing Of Stone");
		this.setManaCost(20 + hero1.getCurrentLevel() * 2);
		this.setHeal(100 + hero1.getCurrentLevel() * 2);
		this.setDamage(20);
	}

	@Override
	public void spell(Hero opponent) {

		if (hero().getCurrentMana() > getManaCost()) {
			hero().setCurrentMana(hero().getCurrentMana() - getManaCost());
			hero().setCurrentHealth(hero().getCurrentHealth() + getHeal());
			opponent.setCurrentHealth(opponent.getCurrentHealth() - getDamage());
			System.out.println(hero().getName() + " enhardened for " + getHeal() + " health!");
		} else {
			System.out.println("Insuficient mana");
			opponent.setCurrentHealth(opponent.getCurrentHealth() - hero().getMinimumDmg());
		}
	}

	@Override
	public void spellBuffer(Hero hero) {
		Buffs buffed = new Shield(hero);
		buffed.buff(hero());
	}

	@Override
	public void spellDebuffer(Hero hero) {
// this spell has no debuff
	}

}
