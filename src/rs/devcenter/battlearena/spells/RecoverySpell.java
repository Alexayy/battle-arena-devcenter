package rs.devcenter.battlearena.spells;

import rs.devcenter.battlearena.buffs.Bless;
import rs.devcenter.battlearena.buffs.Buffs;
import rs.devcenter.battlearena.heroes.Hero;

public class RecoverySpell extends Spells {
	/*
	 * Chanter, balanced heal
	 *
	 * @author Aleksa Cakic
	 */
	public RecoverySpell(Hero hero) {
		this.setHero1(hero);
		this.setName("Recovery Spell");
		this.setManaCost(50 + hero.getCurrentLevel() * 2);
		this.setHeal(100 + hero.getCurrentLevel() * 2);
	}

	@Override
	public void spell(Hero hero) {

		if (hero.getCurrentMana() > getManaCost()) {
			hero.setCurrentMana(hero.getCurrentMana() - getManaCost());
			hero.setCurrentHealth(hero.getCurrentHealth() + getHeal());
			System.out.println(hero().getName() + " healed for " + getHeal() + " health!");
		}
	}

	@Override
	public void spellBuffer(Hero hero) {
		Buffs buffed = new Bless(hero);
		buffed.buff(hero());
	}

	@Override
	public void spellDebuffer(Hero hero) {
// This spell has no debuff
	}

}
