package rs.devcenter.battlearena.spells;

import rs.devcenter.battlearena.heroes.Hero;

/*
 * Gladiator, stun effect
 *
 * @author Aleksa Cakic
 * */
public class RelentlessBlow extends Spells {

	public RelentlessBlow(Hero hero1) {
		this.setHero1(hero1);
		this.setName("Relentless Blow");
		this.setManaCost(20 + hero1.getCurrentLevel() * 2);
		this.setDamage(20 + hero1.getMaximumDmg() + hero1.getCurrentLevel() * 2);
	}

	@Override
	public void spell(Hero hero2) {

		if (hero().getCurrentMana() > getManaCost()) {
			hero().setCurrentMana(hero().getCurrentMana() - getManaCost());
			hero2.setCurrentHealth(hero2.getCurrentHealth() - getDamage());
			System.out.println(hero().getName() + " swung the polearm at " + hero2.getName() + " for "
					+ getDamage() + " damage!");
		} else {
			System.out.println("Insuficient mana");
			hero2.setCurrentHealth(hero2.getCurrentHealth() - hero().getMinimumDmg());
		}
	}

	@Override
	public void spellBuffer(Hero hero) {
		// This spell has no buff

	}

	@Override
	public void spellDebuffer(Hero hero) {
		// This spell has no debuff

	}

}
