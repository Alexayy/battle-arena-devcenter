package rs.devcenter.battlearena.spells;

import rs.devcenter.battlearena.heroes.Hero;

/*
 * Ranger, strongest spell ranger has
 *
 * @author Aleksa Cakic
 * */
public class BullsEye extends Spells {

	public BullsEye(Hero hero1) {
		this.setHero1(hero1);
		this.setName("BullsEye");
		this.setManaCost(150 + hero1.getCurrentLevel() * 2);
		this.setDamage(75 + hero1.getMaximumDmg() + hero1.getCurrentLevel() * 2);
	}

	@Override
	public void spell(Hero hero2) {

		if (hero().getCurrentMana() > getManaCost()) {
			hero().setCurrentMana(hero().getCurrentMana() - getManaCost());
			hero2.setCurrentHealth(hero2.getCurrentHealth() - getDamage());
			System.out.println(hero().getName() + " aimed at " + hero2.getName() + " forhead and did "
					+ getDamage() + " damage!");
		} else {
			System.out.println("Insuficient mana");
			hero2.setCurrentHealth(hero2.getCurrentHealth() - hero().getMinimumDmg());
		}
	}

	@Override
	public void spellBuffer(Hero herof) {
		// This spell has no buff

	}

	@Override
	public void spellDebuffer(Hero herof) {
		// This spell has no debuff

	}

}
