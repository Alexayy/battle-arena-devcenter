package rs.devcenter.battlearena.spells;

import rs.devcenter.battlearena.buffs.Buffs;
import rs.devcenter.battlearena.buffs.Empowerement;
import rs.devcenter.battlearena.heroes.Hero;

/*
 * Gladiator, does more damage
 *
 * @Author Aleksa Cakic
 * */
public class Rage extends Spells {

	public Rage(Hero hero1) {
		this.setHero1(hero1);
		this.setName("Rage");
		this.setManaCost(150 + hero1.getCurrentLevel() * 2);
		this.setDamage(50 + hero1.getMaximumDmg() + hero1.getCurrentLevel() * 2);
	}

	@Override
	public void spell(Hero hero2) {
		if (hero().getCurrentMana() > this.getManaCost()) {
			hero().setCurrentMana(hero().getCurrentMana() - this.getManaCost());
			hero2.setCurrentHealth(hero2.getCurrentHealth() - this.getDamage());
			System.out.println(hero().getName() + " rages and leaps to " + hero2.getName() + " and does "
					+ getDamage() + " damage!");
		} else {
			System.out.println("Insuficient mana");
			hero2.setCurrentHealth(hero2.getCurrentHealth() - hero().getMinimumDmg());
		}
	}

	@Override
	public void spellBuffer(Hero hero) {
		Buffs buffed = new Empowerement(hero);
		buffed.buff(hero());
	}

	@Override
	public void spellDebuffer(Hero hero) {
		// This spell has no debuff

	}

}
