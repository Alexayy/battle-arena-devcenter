package rs.devcenter.battlearena.spells;

import rs.devcenter.battlearena.buffs.Bless;
import rs.devcenter.battlearena.buffs.Buffs;
import rs.devcenter.battlearena.debuffs.DamageOverTime;
import rs.devcenter.battlearena.debuffs.Debuffs;
import rs.devcenter.battlearena.heroes.Hero;

/*
 * Templar, all the HP buffs
 *
 * @author Aleksa Cakic
 * */

public class EmpyreanProvidence extends Spells {

	public EmpyreanProvidence(Hero hero1) {
		this.setHero1(hero1);
		this.setName("Empyrean Providence");
		this.setManaCost(80 + hero1.getCurrentLevel() * 2);
		this.setHeal(50 + hero1.getCurrentLevel() * 2);
	}

	@Override
	public void spell(Hero hero1) {

		if (hero1.getCurrentMana() > getManaCost()) {
			hero().setCurrentMana(hero().getCurrentMana() - getManaCost());
			hero1.setCurrentHealth(hero1.getCurrentHealth() + getHeal());
			System.out.println(hero().getName() + " healed for " + getHeal() + " health!");
		} else {
			System.out.println("Insuficient mana");
			hero1.setCurrentHealth(hero1.getCurrentHealth() - hero().getMinimumDmg());
		}
	}

	@Override
	public void spellBuffer(Hero hero) {
		Buffs buffed = new Bless(hero);
		buffed.buff(hero());
	}

	@Override
	public void spellDebuffer(Hero hero) {
		System.out.println("dot");
		Debuffs debuff = new DamageOverTime(getHero2());
		debuff.debuff(hero);

	}

}
