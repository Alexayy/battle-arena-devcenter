package rs.devcenter.battlearena.spells;

import rs.devcenter.battlearena.debuffs.Debuffs;
import rs.devcenter.battlearena.debuffs.Leach;
import rs.devcenter.battlearena.heroes.Hero;

/*
 * Ranger, Aion's ranger inspiration for Silencing Arrow - leach debuff
 *
 * @author Aleksa Cakic
 * */
public class ThroatShot extends Spells {

	public ThroatShot(Hero hero1) {
		this.setHero1(hero1);
		this.setName("Throat Shot");
		this.setManaCost(100 + hero1.getCurrentLevel() * 2);
		this.setDamage(40 + hero1.getMaximumDmg() + hero1.getCurrentLevel() * 2);
	}

	@Override
	public void spell(Hero hero2) {

		if (hero().getCurrentMana() > getManaCost()) {
			hero().setCurrentMana(hero().getCurrentMana() - getManaCost());
			hero2.setCurrentHealth(hero2.getCurrentHealth() - getDamage());
			System.out.println(hero().getName() + " aimed at " + hero2.getName() + " throat and it did "
					+ getDamage() + " damage!");
		} else {
			System.out.println("Insuficient mana");
			hero2.setCurrentHealth(hero2.getCurrentHealth() - hero().getMinimumDmg());
		}
	}

	@Override
	public void spellBuffer(Hero hero) {
// This spell has no buff
	}

	@Override
	public void spellDebuffer(Hero hero) {
		System.out.println("This spell is leaching powers from others!");
		Debuffs debuff = new Leach(getHero2());
		debuff.debuff(hero);
	}

}
