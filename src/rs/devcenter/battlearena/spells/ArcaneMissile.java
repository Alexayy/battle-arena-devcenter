package rs.devcenter.battlearena.spells;

import rs.devcenter.battlearena.heroes.Hero;

/*
 * Sorcerer, Burst spell - flavor text: Blasts 4 nukes at an enemy
 *
 * @author Aleksa Cakic
 * */
public class ArcaneMissile extends Spells {
	
	public ArcaneMissile(Hero hero1) {
		this.setHero1(hero1);
		this.setName("Arcane Missile");
		this.setManaCost(80 + hero1.getCurrentLevel() * 2);
		this.setDamage(80 + hero1.getMaximumDmg() + hero1.getCurrentLevel() * 2);
	}

	@Override
	public void spell(Hero hero2) {

		if (hero().getCurrentMana() > getManaCost()) {
			hero().setCurrentMana(hero().getCurrentMana() - getManaCost());
			hero2.setCurrentHealth(hero2.getCurrentHealth() - getDamage());
			System.out.println(hero().getName() + " blasted " + hero2.getName() + " for "

					+ getDamage() + " damage!");
		} else {
			System.out.println("Insuficient mana");
			hero2.setCurrentHealth(hero2.getCurrentHealth() - hero().getMinimumDmg());
		}
	}

	@Override
	public void spellBuffer(Hero hero) {
		// This spell has no buff
	}

	@Override
	public void spellDebuffer(Hero hero) {
		// This spell has no debuff
	}
	
}
