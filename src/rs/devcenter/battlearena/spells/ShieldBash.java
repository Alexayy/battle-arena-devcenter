package rs.devcenter.battlearena.spells;

import rs.devcenter.battlearena.debuffs.Debuffs;
import rs.devcenter.battlearena.debuffs.Leach;
import rs.devcenter.battlearena.heroes.Hero;

/*
 * Templar, Leach
 *
 * @author Aleksa Cakic
 */

public class ShieldBash extends Spells {

	public ShieldBash(Hero hero1) {
		this.setHero1(hero1);
		this.setName("Shield Bash");
		this.setManaCost(20 + hero1.getCurrentLevel() * 2);
		this.setDamage(10 + hero1.getMaximumDmg() + hero1.getCurrentLevel() * 2);
	}

	@Override
	public void spell(Hero hero2) {

		if (hero().getCurrentMana() > getManaCost()) {
			hero().setCurrentMana(hero().getCurrentMana() - getManaCost());
			hero2.setCurrentHealth(hero2.getCurrentHealth() - getDamage());
			System.out.println(hero().getName() + " bashed " + hero2.getName() + " and did "
					+ getDamage() + " damage!");
		} else {
			System.out.println("Insuficient mana");
			hero2.setCurrentHealth(hero2.getCurrentHealth() - hero().getMinimumDmg());
		}
	}

	@Override
	public void spellBuffer(Hero hero) {
		// This spell has no buffs

	}

	@Override
	public void spellDebuffer(Hero hero) {
		System.out.println("This spell is leaching powers from others!");
		Debuffs debuff = new Leach(getHero2());
		debuff.debuff(hero);
	}
}
