package rs.devcenter.battlearena.spells;

import rs.devcenter.battlearena.heroes.*;

/*
 * Ranger spell
 *
 * @author Aleksa Cakic
 * */

public class AnkleShot extends Spells {

	public AnkleShot(Hero hero1) {
		this.setHero1(hero1);
		this.setName("Ankle Shot");
		this.setManaCost(40 + hero1.getCurrentLevel() * 2);
		this.setDamage(50 + hero1.getMaximumDmg() + hero1.getCurrentLevel() * 2);
	}

	@Override
	public void spell(Hero hero2) {

		if (hero().getCurrentMana() > getManaCost()) {
			hero().setCurrentMana(hero().getCurrentMana() - getManaCost());
			hero2.setCurrentHealth(hero2.getCurrentHealth() - getDamage());
			System.out.println(hero().getName() + " aimed at " + hero2.getName() + " ankle and it did "

					+ getDamage() + " damage!");
		} else {
			System.out.println("Insuficient mana");
			hero2.setCurrentHealth(hero2.getCurrentHealth() - hero().getMinimumDmg());
		}
	}

	@Override
	public void spellBuffer(Hero hero) {
		// null, this spell has no buff
	}

	@Override
	public void spellDebuffer(Hero hero) {
		// null, this spell has no debuffs
	}
	
}
