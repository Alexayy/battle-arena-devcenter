package rs.devcenter.battlearena.spells;

import rs.devcenter.battlearena.buffs.Buffs;
import rs.devcenter.battlearena.buffs.Empowerement;
import rs.devcenter.battlearena.heroes.Hero;

/*
 * Gladiator - flavor: Leaps in the air, after he lands, earth shivers and shatters
 *
 * @author Aleksa Cakic
 * */
public class BerserkersDash extends Spells {

	public BerserkersDash(Hero hero1) {
		this.setHero1(hero1);
		this.setName("BerserkersDash");
		this.setManaCost(80 + hero1.getCurrentLevel() * 2);
		this.setDamage(25 + hero1.getMaximumDmg() + hero1.getCurrentLevel() * 2);
	}

	@Override
	public void spell(Hero hero2) {

		if (hero().getCurrentMana() > getManaCost()) {
			hero().setCurrentMana(hero().getCurrentMana() - getManaCost());
			hero2.setCurrentHealth(hero2.getCurrentHealth() - getDamage());
			System.out.println(hero().getName() + " leaps to " + hero2.getName() + " and does "
					+ getDamage() + " damage!");
		} else {
			System.out.println("Insuficient mana");
			hero2.setCurrentHealth(hero2.getCurrentHealth() - hero().getMinimumDmg());
		}
	}

	@Override
	public void spellBuffer(Hero hero) {
		Buffs buffed = new Empowerement(hero);
		buffed.setAmount(50);
		buffed.buff(hero());
	}

	@Override
	public void spellDebuffer(Hero hero) {
		// This spell has no debuff
	}
}
