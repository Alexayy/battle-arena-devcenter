package rs.devcenter.battlearena.Modes;

import rs.devcenter.battlearena.combat.PlayerVsOpponent;
import rs.devcenter.battlearena.debuffs.Curse;
import rs.devcenter.battlearena.debuffs.Debuffs;
import rs.devcenter.battlearena.heroes.Hero;
/*
* This class brings absolute chaos, testing players patience, and for some this kind of
* "super boss" enemy and little bit of HP and power buffs, makes tad more fun.
*
* @author Aleksa Cakic
* */
public class NotThatFairIGuess extends Mode {

    public NotThatFairIGuess (Hero hero1, Hero hero2) {
        this.setHero1(hero1);
        this.setHero2(hero2);
        this.setName(" FITE ME >:DD");
    }

    @Override
    public void combat() {
        PlayerVsOpponent playerVsOpponent = new PlayerVsOpponent(getHero1(), getHero2());

        getHero1().setCurrentLevel(50);
        getHero1().setMaxMana(1000);
        getHero1().setMaximumDmg(getHero1().getMaximumDmg() + 100);
        getHero1().setMaxHealth(1000);
        getHero1().setCurrentHealth(1000);

        getHero2().setCurrentLevel(100);
        getHero2().setMaxHealth(getHero2().getCurrentHealth() * 100);
        getHero2().setCurrentHealth(getHero2().getCurrentHealth() * 100);
        getHero2().setMaxMana(getHero2().getCurrentMana() * 100);
        getHero2().setMaximumDmg(1);

        System.out.println("Hero: " + getHero1().getName() + ", HP: " + getHero1().getMaxHealth() + ", MP: "
                + getHero1().getCurrentMana());
        System.out.println("Hero: " + getHero2().getName() + ", HP: " + getHero2().getMaxHealth() + ", MP: "
                + getHero2().getCurrentMana());

        /*
        * While loop that will run the game untill one of the heroes drops to 0 HP
        * */
        while (getHero1().getCurrentHealth() > 0 && getHero2().getCurrentHealth() > 0) {
            playerVsOpponent.player();
            getHero2().setCurrentHealth(getHero2().getCurrentHealth() - 100);
            playerVsOpponent.opponent();
            getHero1().setCurrentHealth(getHero1().getCurrentHealth() - 50);

        }

        if (getHero1().getCurrentHealth() < 0)
            System.out.println("This was a bit of an overkill for you, much like this whole project for me. :D ");
        else if (getHero2().getCurrentHealth() < 0)
            System.out.println("Congrats! You finished this nightmare much like i finished mine, this game! :D");
    }
/*
* This method will be triggered if this mode was held in Arena of Chaos
* */
    @Override
    public void debuff() {
        Debuffs debuffed = new Curse(getHero1(), getHero2());
        debuffed.debuffStrenght(100);
    }
}
