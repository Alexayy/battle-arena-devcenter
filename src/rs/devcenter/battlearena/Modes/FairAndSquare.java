package rs.devcenter.battlearena.Modes;

import rs.devcenter.battlearena.combat.PlayerVsOpponent;
import rs.devcenter.battlearena.debuffs.Curse;
import rs.devcenter.battlearena.debuffs.Debuffs;
import rs.devcenter.battlearena.heroes.Hero;
/*
* Fair and square is classical 1v1 battle mode.
*
* @author Aleksa Cakic
* */
public class FairAndSquare extends Mode {

	public FairAndSquare(Hero hero1, Hero hero2) {
		this.setHero1(hero1);
		this.setHero2(hero2);
		this.setName(" Fair and Square 1v1 :D ");
	}

	/*
	* Combat takes 2 heroes and make them fight.
	* */
	@Override
	public void combat() {

		PlayerVsOpponent playerVsOpponent = new PlayerVsOpponent(getHero1(), getHero2());

		System.out.println("Hero: " + getHero1().getName() + ", HP: " + getHero1().getMaxHealth() + ", MP: "
				+ getHero1().getCurrentMana());
		System.out.println("Hero: " + getHero2().getName() + ", HP: " + getHero2().getMaxHealth() + ", MP: "
				+ getHero2().getCurrentMana());

		/*
		* It will loop untill one drops to 0
		* */
		while (getHero1().getCurrentHealth() > 0 && getHero2().getCurrentHealth() > 0) {

			playerVsOpponent.player();
			if (getHero1().getCurrentHealth() < 0) {
				getHero2().setCurrentXp(getHero2().getCurrentXp() + 1);
				break;
			}
			playerVsOpponent.opponent();
			if (getHero2().getCurrentHealth() < 0) {
				getHero1().setCurrentXp(getHero1().getCurrentXp() + 2);
				getHero2().setCurrentXp(getHero2().getCurrentXp() + 1);
				break;
			}
		}

		if (getHero1().getCurrentHealth() < 0)
			System.out.println("Oh boi, you just lost to a " + getHero2().getName() + "?");
		
		else if (getHero2().getCurrentHealth() < 0)
			System.out.println("Damn son, you just annihilated " + getHero2().getName() + "!");
		
		else
			System.out.println("It's not a bow tie, it's a tie!");

	}
/*
* In case fair 1v1 is called in Chaos arena, this method will trigger
* */
	@Override
	public void debuff() {
		Debuffs debuffed = new Curse(getHero1(), getHero2());
		debuffed.debuffStrenght(100);
	}
}
