package rs.devcenter.battlearena.Modes;

import rs.devcenter.battlearena.heroes.Hero;
/*
* This is an abstract class that will be inherited by FairAndSquare and NotThatFairIGuess classes.
*
* @author Aleksa Cakic
* */
public abstract class Mode {

	private String name;
	private Hero hero1, hero2;

	public abstract void combat();

	public abstract void debuff();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Hero getHero1() {
		return hero1;
	}

	public void setHero1(Hero hero1) {
		this.hero1 = hero1;
	}

	public Hero getHero2() {
		return hero2;
	}

	public void setHero2(Hero hero2) {
		this.hero2 = hero2;
	}

}
