package rs.devcenter.battlearena.arenas;

import rs.devcenter.battlearena.Modes.FairAndSquare;
import rs.devcenter.battlearena.Modes.NotThatFairIGuess;
import rs.devcenter.battlearena.debuffs.Curse;
import rs.devcenter.battlearena.heroes.Hero;

import java.util.Scanner;
/*
* Arena of Discipline is located in main city of Sanctum in Atreia. People gather to watch honorous fight
* between bravest of heroes!
*
* @author Aleksa Cakic
* */
public class ArenaOfDiscipline extends Arena {

	public ArenaOfDiscipline(Hero hero1, Hero hero2) {
		this.setHero1(hero1);
		this.setHero2(hero2);
		this.setName("Arena of Discipline");
		this.setDescription("Arena that greets you upon entrance!");
		this.setMode(getMode());
	}
	/*
	 * hostFight() is the method that stores and calls two battle modes from which the player can chose.
	 * */
	@Override
	public void hostFight() {
		Scanner sc = new Scanner(System.in);
		System.out.println("\nPress 1 if you would like a fair fight, or perhaps 2 if you adore true CHAOS!");
		System.out.println(getDescription());
		int choose = sc.nextInt();

		switch (choose) {
		case 1:
			FairAndSquare fairAndSquare = new FairAndSquare(getHero1(), getHero2());
			fairAndSquare.combat();
			break;
		case 2:
			NotThatFairIGuess ntfig = new NotThatFairIGuess(getHero1(), getHero2());
			ntfig.combat();
			break;
		}
	}
}
