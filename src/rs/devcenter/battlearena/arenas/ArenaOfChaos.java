package rs.devcenter.battlearena.arenas;

import rs.devcenter.battlearena.Modes.FairAndSquare;
import rs.devcenter.battlearena.Modes.NotThatFairIGuess;
import rs.devcenter.battlearena.debuffs.Curse;
import rs.devcenter.battlearena.heroes.Hero;

import java.util.Scanner;

/*
* Arena of Chaos is a rocky Colliseum somehwere on a volcano. Constant heat weakens
* hearos and exausts them. All heros start with 100 HP less.
*
* @author Aleksa Cakic
* */

public class ArenaOfChaos extends Arena {

	public ArenaOfChaos(Hero hero1, Hero hero2) {
		this.setHero1(hero1);
		this.setHero2(hero2);
		this.setName("Arena of Chaos");
		Curse curse = new Curse(hero1, hero2);
		setDebuffs(curse);
		this.setDescription("\nLava, rocky mountain, the landscape of fears," + "\nHeat that bites and evaporates tears, "
				+ "\nAll Heroes shall be put to the test, " + "\nAnd the one who beats them all will be the best!");
	}

	/*
	* hostFight() is the method that stores and calls two battle modes from which the player can chose.
	* */

	@Override
	public void hostFight() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Press 1 if you would like a fair fight, or perhaps 2 if you adore true CHAOS!");
		System.out.println(this.getDescription());
		int choose = sc.nextInt();
		switch (choose) {

			case 1:
			FairAndSquare fairAndSquare = new FairAndSquare(getHero1(), getHero2());
			Curse cursed = new Curse(getHero1(), getHero2());
			cursed.arenaDebuff(getHero1(), getHero2());
			fairAndSquare.combat();
			break;

			case 2:
			NotThatFairIGuess ntfig = new NotThatFairIGuess(getHero1(), getHero2());
			Curse curse = new Curse(getHero1(), getHero2());
			curse.arenaDebuff(getHero1(), getHero2());
			ntfig.combat();
			break;
		}
	}
}
