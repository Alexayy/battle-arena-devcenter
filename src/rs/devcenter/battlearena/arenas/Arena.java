package rs.devcenter.battlearena.arenas;

import rs.devcenter.battlearena.Modes.Mode;
import rs.devcenter.battlearena.buffs.Buffs;
import rs.devcenter.battlearena.debuffs.Debuffs;
import rs.devcenter.battlearena.heroes.Hero;
/*
* Abstract class from which Arena of Chaos and Arena of Discipline will inherit it's properties and
* implement all abstract methods
*
* @author Aleksa Cakic
* */
public abstract class Arena {

	private String name;
	private Hero hero1, hero2;
	private Buffs buff;
	private Debuffs debuffs;
	private Mode mode;
	private String description;

	public abstract void hostFight();

	public Mode getMode() {
		return mode;
	}

	public void setMode(Mode mode) {
		this.mode = mode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Hero getHero1() {
		return hero1;
	}

	public void setHero1(Hero hero1) {
		this.hero1 = hero1;
	}

	public Hero getHero2() {
		return hero2;
	}

	public void setHero2(Hero hero2) {
		this.hero2 = hero2;
	}

	public Buffs getBuff() {
		return buff;
	}

	public void setBuff(Buffs buff) {
		this.buff = buff;
	}

	public Debuffs getDebuffs() {
		return debuffs;
	}

	public void setDebuffs(Debuffs debuffs) {
		this.debuffs = debuffs;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
