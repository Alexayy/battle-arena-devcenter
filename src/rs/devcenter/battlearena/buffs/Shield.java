package rs.devcenter.battlearena.buffs;

import rs.devcenter.battlearena.heroes.Hero;

/*
 * Shield is a permanent add to current and max HP.
 *
 * @author Aleksa Cakic
 * */
public class Shield extends Buffs {

	public Shield(Hero hero1, Hero hero2) {
		this.setHero1(hero1);
		this.setHero2(hero2);
		this.setName("Shield");
	}

	public Shield(Hero hero1) {
		this.setHero1(hero1);
		this.setName("Shield");
	}

	@Override
	public void buff(Hero hero) {
			hero.setMaxHealth(hero.getMaxHealth() + 100);
			hero.setCurrentHealth(hero.getCurrentHealth() + 100);

		if (hero.getCurrentHealth() < hero.getCurrentHealth() + 100) {
			hero.setCurrentHealth(hero.getCurrentHealth() - 100);
			System.out.println("Shield lost!");
		}
	}
}
