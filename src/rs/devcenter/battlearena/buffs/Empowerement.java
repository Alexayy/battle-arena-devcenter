package rs.devcenter.battlearena.buffs;

import rs.devcenter.battlearena.heroes.Hero;

/*
* Empowerement is a buff that boosts heroes Maximal Damage which is a modifier to add more damage!
*
* @author Aleksa Cakic
* */
public class Empowerement extends Buffs {

	public Empowerement(Hero hero1, Hero hero2) {
		this.setHero1(hero1);
		this.setHero2(hero2);
		this.setName("Empowerment");
		this.setAmount(0);
	}

	public Empowerement(Hero hero1) {
		this.setHero1(hero1);
		this.setName("Empowerment");
		this.setAmount(0);
		this.setTrigger(false);
	}

	@Override
	public void buff(Hero hero) {
			hero.setMaximumDmg(hero.getMaximumDmg() + getAmount());
			hero.setMinimumDmg(hero.getMinimumDmg() + getAmount());
			this.setTrigger(true);
	}
}
