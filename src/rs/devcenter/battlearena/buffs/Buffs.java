package rs.devcenter.battlearena.buffs;

import rs.devcenter.battlearena.heroes.Hero;
/*
* Abstarct class from which Bless, Empowerement and Shield class will inherit properties and implement
* abstract methods.
*
* @author Aleksa Cakic
* */
public abstract class Buffs {

	private Hero hero1, hero2;
	private String name;
	private int amount;
	private boolean trigger;

	public abstract void buff(Hero hero);

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public Hero getHero1() {
		return hero1;
	}

	public void setHero1(Hero hero1) {
		this.hero1 = hero1;
	}

	public Hero getHero2() {
		return hero2;
	}

	public void setHero2(Hero hero2) {
		this.hero2 = hero2;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean getTrigger() {
		return trigger;
	}

	public void setTrigger(boolean trigger) {
		this.trigger = trigger;
	}

}
