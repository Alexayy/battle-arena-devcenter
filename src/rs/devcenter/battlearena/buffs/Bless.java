package rs.devcenter.battlearena.buffs;
import rs.devcenter.battlearena.heroes.Hero;

public class Bless extends Buffs {
/*
* Bless is an extra boost to heal, it's heal AND additional health.
*
* @author Aleksa Cakic
* */
	public Bless(Hero hero1, Hero hero2) {
		this.setHero1(hero1);
		this.setHero2(hero2);
		this.setName("Bless");
	}

	public Bless(Hero hero) {
		this.setHero1(hero);
		this.setName("Bless");
		this.setTrigger(false);
	}

	@Override
	public void buff(Hero hero) {
			hero.setMaxHealth(hero.getMaxHealth() + 100);
			hero.setCurrentHealth(hero.getCurrentHealth() + 100);
	}
}
