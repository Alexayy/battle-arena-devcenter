package rs.devcenter.battlearena;

import rs.devcenter.battlearena.arenas.ArenaOfChaos;
import rs.devcenter.battlearena.arenas.ArenaOfDiscipline;
import rs.devcenter.battlearena.heroes.*;

import java.util.Random;
import java.util.Scanner;
/*
* This class has the execute() method that makes this game runnable.
*
* @author Aleksa Cakic
* */
public class HeroSelection {

	private Hero hero1, hero2;

	private Scanner pickPlayer = new Scanner(System.in);
	private Scanner chooseArena = new Scanner(System.in);
	private Random pickOpponent = new Random();

	public void pickArena() {
// Selection of 2 arenas
		System.out.println("Choose the arena you will be fighting in!" +
				"Press 1 for Arena of Chaos, or 2 for Arena of Discipline!");

		int selectArena = chooseArena.nextInt();

		if (selectArena == 1) {
			System.out.println("\nARENA OF CHAOS");
			ArenaOfChaos arenaOfChaos = new ArenaOfChaos(hero1, hero2);
			arenaOfChaos.hostFight();

		} if (selectArena == 2) {
			System.out.println("\nARENA OF DISCIPLINE");
			ArenaOfDiscipline aod = new ArenaOfDiscipline(hero1, hero2);
			aod.hostFight();
		}
	}

	public void pickHero() {

// Hero selection
		System.out.println("Select your hero!");
		System.out.println("\nPress 1 to select a ubreakable Chanter!"
		+  "\nPress 2 to select an unstopable Gladiator!"
		+  "\nPress 3 to select a cunning Ranger!"
		+  "\nPress 4 to select a destructive Sorcerer!"
		+  "\nPress 5 to select a resilient Templar!");
		int selHero = pickPlayer.nextInt();

		if (selHero == 1) {
			Chanter chanter = new Chanter("Chanter", 0, 1);
			System.out.println("You've selected a Chanter!");
			hero1 = chanter;

		} else if (selHero == 2) {
			Gladiator gladiator = new Gladiator("Gladiator", 0, 1);
			System.out.println("You've selected a Gladiator!");
			hero1 = gladiator;

		} else if (selHero == 3) {
			Ranger ranger = new Ranger("Ranger", 0, 1);
			System.out.println("You've selected a Ranger!");
			hero1 = ranger;

		} else if (selHero == 4) {
			Sorcerer sorcerer = new Sorcerer("Sorcerer", 0, 1);
			System.out.println("You've selected a Sorcerer!");
			hero1 = sorcerer;

		} else if (selHero == 5) {
			Templar templar = new Templar("Templar", 0, 1);
			System.out.println("You've selected a Templar!");
			hero1 = templar;
		}

// Randomly selected opponent
		int pickOppo = pickOpponent.nextInt(3) + 1;
		System.out.println("\nYour opponenet is selected randomly!");

		if (pickOppo == 0) {
			Chanter chanter = new Chanter("Chanter", 0, 1);
			System.out.println("Your opponent is a Chanter!");
			hero2 = chanter;
		} else if (pickOppo == 1) {
			System.out.println("Your opponent is a Gladiator!");
			hero2 = new Gladiator("Gladiator", 0, 1);
		} else if (pickOppo == 2) {
			System.out.println("Your opponent is a Ranger!");
			hero2 = new Ranger("Ranger", 0, 1);
		} else if (pickOppo == 3) {
			System.out.println("Your opponent is a Sorcerer!");
			hero2 = new Sorcerer("Sorcerer", 0, 1);
		} else if (pickOppo == 4) {
			System.out.println("Your opponent is a Templar!");
			hero2 = new Templar("Templar", 0, 1);
		}
	}

	public void execute(){

		this.pickHero(); // calls pickHero()
		this.pickArena(); // calls pickArena()

	}
}
